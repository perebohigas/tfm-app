import RxSwift

class TariffManagerCoordinator: BaseCoordinator {
  private let disposeBag = DisposeBag()
  private let viewModel: TariffManagerViewModel
  
  init(session activeSession: Session) {
    let tariffManagerViewModel = TariffManagerViewModel(session: activeSession)
    self.viewModel = tariffManagerViewModel
  }
  
  init?(session activeSession: Session, tariffId: String) {
    guard let tariffManagerViewModel = TariffManagerViewModel(session: activeSession, tariffId: tariffId) else { return nil }
    self.viewModel = tariffManagerViewModel
  }
  
  override func start() {
    let viewController = TariffManagerViewController.instantiate()
    viewController.viewModel = self.viewModel
    viewController.modalPresentationStyle = .pageSheet
    self.navigationController.present(viewController, animated: true)
    self.setUpBindings()
  }
  
  func dismiss(session activeSession: Session) {
    if let tariffsCoordinator = self.parentCoordinator as? TariffsCoordinator {
      tariffsCoordinator.reloadData(session: activeSession)
    }
    self.parentCoordinator?.release(coordinator: self)
  }
  
  private func setUpBindings() {
    self.viewModel.didDismiss
      .observeOn(MainScheduler.instance)
      .bind { [unowned self] session in
        self.dismiss(session: session)
      }
      .disposed(by: disposeBag)
  }
}
