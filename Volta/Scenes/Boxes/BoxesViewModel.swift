import Foundation
import RxSwift
import RxRelay

class BoxesViewModel {
  let disposeBag = DisposeBag()
  var activeSession: Session
  
  // UI
  var displayedBoxes: [Box]
  var displayedTotalValues: BoxesTotalValues
  let tableDataSource = BehaviorSubject(value: [Box]())
  let totalsDataSource = BehaviorSubject(value: BoxesTotalValues())
  
  // Events
  let isPolling = BehaviorRelay<Bool>(value: false)
  let isLoading = BehaviorRelay<Bool>(value: false)
  let isBoxSelectedForContent = PublishSubject<(Session, String)>()
  let isBoxSelectedForInformation = PublishSubject<(Session, String)>()
  let isShowingTariffsManager = PublishSubject<(Session)>()
  
  init(session: Session) {
    self.activeSession = session
    self.displayedBoxes = [Box]()
    self.displayedTotalValues = BoxesTotalValues()
    
    self.setUpBindings()
    self.loadBoxes()
    
    #if MOCKSERVER
    self.addFakeData()
    #endif
  }
  
  private func addFakeData() {
    var fakeTariff1 = ElectricityTariff(
      label: "Easy home",
      provider: "Electric company",
      currency: self.activeSession.user.locale.currencyCode ?? "")
    
    fakeTariff1.consumptionPrice = 0.286
    fakeTariff1.productionRevenue = 0.185
    fakeTariff1.fixedRate = 210
    
    self.activeSession.user.tariffs[fakeTariff1.id] = fakeTariff1
    self.activeSession.user.boxes["1"]?.associatedTariffId = fakeTariff1.id
    self.activeSession.user.boxes["3"]?.associatedTariffId = fakeTariff1.id
    
    var fakeTariff2 = ElectricityTariff(
      label: "Business pro",
      provider: "Power enterprise",
      usage: .industrial,
      currency: self.activeSession.user.locale.currencyCode ?? "")
    
    fakeTariff2.consumptionPrice = 0.321
    fakeTariff2.productionRevenue = 0.24
    fakeTariff2.fixedRate = 185
    
    self.activeSession.user.tariffs[fakeTariff2.id] = fakeTariff2
    self.activeSession.user.boxes["2"]?.associatedTariffId = fakeTariff2.id
    self.activeSession.user.boxes["4"]?.associatedTariffId = fakeTariff2.id
  }
  
  private func setUpBindings() {
    self.isPolling
      .flatMapLatest {  isPolling in
        isPolling ? Observable<Int>.interval(.seconds(1), scheduler: MainScheduler.instance) : .empty()
    }
    .bind { _ in self.fetchBoxesCurrentPowerData() }
    .disposed(by: self.disposeBag)
  }
  
  func loadBoxes() {
    Observable
      .just(BoxesInformationRequest())
      .do { self.isLoading.accept(true) }
      .flatMapLatest { [weak self] request -> Single<(Int, BoxesInformationResponse)> in
        return (self?.activeSession.restClient.request(with: request))!
    }
    .subscribe(
      onNext: { [weak self] _, response in
        self?.activeSession.user.boxes = response.data.reduce(into: [String: Box]()) {
          $0[$1.id] = $1
        }
        self?.loadDevices()
      }, onError: { error in
        print("An error ocurred while a request was being made: \(error)")
    }
    )
      .disposed(by: self.disposeBag)
  }
  
  func loadDevices() {
    for boxId in self.activeSession.user.boxes.keys {
      Observable
        .just(DevicesInformationRequest(boxId: boxId))
        .do { self.isLoading.accept(true) }
        .flatMapLatest { [weak self] request -> Single<(Int, DevicesInformationResponse)> in
          return (self?.activeSession.restClient.request(with: request))!
      }
      .subscribe(
        onNext: { [weak self] _, response in
          let receivedDevices = response.data.reduce(into: [String: Device]()) {
            $0[$1.id] = $1
          }
          self?.activeSession.user.boxes[boxId]?.devices = receivedDevices
          self?.isLoading.accept(false)
        },
        onError: { error in
          print("An error ocurred while a request was being made: \(error)")
      }
      )
        .disposed(by: self.disposeBag)
    }
  }
  
  func fetchBoxesCurrentPowerData() {
    Observable
      .just(BoxesCurrentPowerRequest())
      .flatMapLatest { [weak self] request -> Single<(Int, BoxesCurrentPowerResponse)> in
        (self?.activeSession.restClient.request(with: request))!
    }
    .subscribe(
      onNext: { [weak self] _, response in
        self?.loadBoxesReceivedPowerData(receivedPowerData: response.data)
      }, onError: { error in
        print("An error ocurred while a request was being made: \(error)")
    }
    )
      .disposed(by: self.disposeBag)
  }
  
  func loadBoxesReceivedPowerData(receivedPowerData: [SingleBoxCurrentPowerResponse]) {
    var needsUpdate: Bool = false
    _ = receivedPowerData.map { (singleBoxPowerData: SingleBoxCurrentPowerResponse) -> () in
      if let existingBox = self.activeSession.user.boxes[singleBoxPowerData.boxId] {
        if !existingBox.currentPower.contains(singleBoxPowerData.currentPower) {
          self.activeSession.user.boxes[singleBoxPowerData.boxId]!.lastTimeActive = singleBoxPowerData.lastTimeActive
          self.activeSession.user.boxes[singleBoxPowerData.boxId]!.currentPower.append(singleBoxPowerData.currentPower)
          needsUpdate = true
        }
      }
    }
    if needsUpdate {
      self.updateTableData(boxesNewStatus: self.activeSession.user.boxes.values.map{ $0 })
      self.updateTotalValues()
    }
  }
  
  func updateTotalValues() {
    self.displayedTotalValues = BoxesTotalValues()
    _ = self.activeSession.user.boxes.values.map { (singleBox: Box) ->() in
      guard let lastValue = self.activeSession.user.getLastPowerValue(boxId: singleBox.id) else { return }
      
      var tariff: ElectricityTariff? = nil
      var fixCost: Amount? = nil
      if let tariffId = singleBox.associatedTariffId, let associatedTariff = activeSession.user.tariffs[tariffId] {
        tariff = associatedTariff
        fixCost = associatedTariff.getConnectionCostForHour(timestamp: lastValue.timestamp)
      }
      
      if lastValue.power.value > 0 {
        // Box producing
        switch lastValue.power.unit {
        case .active:
          self.displayedTotalValues.production.active.value += abs(lastValue.power.value)
        case .reactive:
          // not used
          break
        case .apparent:
          self.displayedTotalValues.production.apparent.value += abs(lastValue.power.value)
        }
        if tariff != nil {
          // Calculate revenue
          let variableRevenue = tariff!.getProductionAmount(powerValue: lastValue.power, timestamp: lastValue.timestamp)
          let currentBalance = calculateBoxBalance(fix: fixCost, variable: variableRevenue)
          self.displayedTotalValues.production.revenue.value += currentBalance.value
        }
      } else if lastValue.power.value <= 0 {
        // Box consuming or not active
        switch lastValue.power.unit {
        case .active:
          self.displayedTotalValues.consumption.active.value += abs(lastValue.power.value)
        case .reactive:
          // not used
          break
        case .apparent:
          self.displayedTotalValues.consumption.apparent.value += abs(lastValue.power.value)
        }
        // Add connection cost (fix cost)
        if tariff != nil {
          // Calculate cost
          let variableCost = tariff!.getConsumptionAmount(powerValue: lastValue.power, timestamp: lastValue.timestamp)
          let currentBalance = calculateBoxBalance(fix: fixCost, variable: variableCost)
          self.displayedTotalValues.consumption.cost.value += currentBalance.value
        }
      }
    }
    
    self.totalsDataSource.onNext(displayedTotalValues)
  }
  
  func calculateBoxBalance(fix: Amount?, variable: Amount?) -> Amount {
    let total: Amount
    if fix != nil {
      if variable != nil {
        total = fix! + variable!
      } else {
        total = fix!
      }
    } else {
      if variable != nil {
        total = variable!
      } else {
        total = Amount(value: 0, currencySymbol: "")
      }
    }
    return total
  }
  
  func updateTableData(boxesNewStatus: [Box]) {
    self.displayedBoxes = boxesNewStatus.sorted()
    self.tableDataSource.onNext(self.displayedBoxes)
  }
  
  func selectBoxForContent(_ position: Int) {
    print("Showing content of box: \(self.displayedBoxes[position].id) - \(self.displayedBoxes[position].label ?? "")")
    self.isBoxSelectedForContent.onNext((self.activeSession, self.displayedBoxes[position].id))
  }
  
  func selectBoxForInfo(_ position: Int) {
    print("Showing information of box: \(self.displayedBoxes[position].id) - \(self.displayedBoxes[position].label ?? "")")
    self.isBoxSelectedForInformation.onNext((self.activeSession, self.displayedBoxes[position].id))
  }
  
  func showTariffsScene() {
    print("Showing tariffs scene")
    self.isShowingTariffsManager.onNext((self.activeSession))
  }
}

struct BoxesTotalValues {
  var production: (active: PowerValue, apparent: PowerValue, revenue: Amount)
  var consumption: (active: PowerValue, apparent: PowerValue, cost: Amount)
  
  init() {
    let emptyActivePowerValue = PowerValue(value: 0.0, type: PowerType.active)
    let emptyApparentPowerValue = PowerValue(value: 0.0, type: PowerType.apparent)
    let emptyAmount = Amount(value: 0, currencySymbol: "")
    
    self.production = (active: emptyActivePowerValue, apparent: emptyApparentPowerValue, revenue: emptyAmount)
    self.consumption = (active: emptyActivePowerValue, apparent: emptyApparentPowerValue, cost: emptyAmount)
  }
}
