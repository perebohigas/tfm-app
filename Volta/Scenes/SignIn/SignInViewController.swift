import UIKit
import RxSwift

class SignInViewController: UIViewController, Storyboarded {
  static var storyboard = AppStoryboard.signIn
  private let disposeBag = DisposeBag()
  var viewModel: SignInViewModel?
  
  // UI elements
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var buttonBackground: UIView!
  @IBOutlet weak var signInButton: UIButton!
  
  // UI constraints
  @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
  @IBOutlet weak var logoBackgroundConstraint: NSLayoutConstraint!
  
  override func viewDidLoad() {
    self.view.backgroundColor = AppColors.mainLightBlack.lighter(by: 69)
    
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(self.keyboardNotification(notification:)),
      name: UIResponder.keyboardWillChangeFrameNotification,
      object: nil
    )
    
    self.setUpBindings()
    self.viewModel?.isSignInActive.accept(false)
    super.viewDidLoad()
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  @objc func keyboardNotification(notification: NSNotification) {
    if let userInfo = notification.userInfo {
      let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
      let endFrameY = endFrame?.origin.y ?? 0
      let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
      let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
      let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
      let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
      if endFrameY >= UIScreen.main.bounds.size.height {
        self.bottomConstraint.constant = 0.0
        self.buttonBackground.isHidden = false
      } else {
        self.bottomConstraint.constant = endFrame?.size.height ?? 0.0
        self.buttonBackground.isHidden = true
      }
      UIView.animate(
        withDuration: duration,
        delay: TimeInterval(0),
        options: animationCurve,
        animations: { self.view.layoutIfNeeded() },
        completion: nil
      )
    }
  }
  
  private func setUpBindings() {
    guard let viewModel = viewModel else { return }
    
    self.usernameTextField.rx.text.orEmpty
      .bind(to: viewModel.userName)
      .disposed(by: self.disposeBag)
    
    self.passwordTextField.rx.text.orEmpty
      .bind(to: viewModel.password)
      .disposed(by: self.disposeBag)
    
    self.signInButton.rx.tap
      .bind { viewModel.signIn() }
      .disposed(by: self.disposeBag)
    
    viewModel.isSignInActive
      .bind {status in
        self.signInButton.isEnabled = status
        if status {
          self.signInButton.alpha = 1.0
        } else {
          self.signInButton.alpha = 0.3
        }
      }
      //            .bind(to: self.signInButton.rx.isEnabled)
      .disposed(by: self.disposeBag)
    
    viewModel.isLoading
      .observeOn(MainScheduler.instance)
      .bind { [weak self] in
        guard let signInViewController = self else { return }
        signInViewController.usernameTextField.isEnabled = !$0
        signInViewController.passwordTextField.isEnabled = !$0
        signInViewController.signInButton.isEnabled = !$0
      }
      .disposed(by: self.disposeBag)
  }
}
