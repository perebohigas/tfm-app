import UIKit
import RxSwift
import RxCocoa
import MapKit

class DetailsViewController: UIViewController, Storyboarded {
  
  static var storyboard = AppStoryboard.details
  private let disposeBag = DisposeBag()
  
  var viewModel: DetailsViewModel?
  
  @IBOutlet weak var cancelButton: UIButton!
  @IBOutlet weak var doneButton: UIButton! {
    didSet {
      doneButton.isEnabled = false
      doneButton.alpha = 0.0
    }
  }
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  
  var selectedRow: Int = 0
  
  override func viewDidLoad() {
    titleLabel.text = viewModel?.displayedVoltaElement.element.description
    
    //self.view.backgroundColor = AppColors.mainLightBlack.lighter(by: 69)
    self.setUpBindings()
    super.viewDidLoad()
  }
  
  private func setUpBindings() {
    guard let viewModel = self.viewModel, let tableView = self.tableView else { return }
    
    viewModel.tableDataSource
      .bind(to: tableView.rx.items) { table, index, property in
        var value: String?
        var name: String?
        switch property.name {
        case "location":
          if let location = property.value as? Location {
            return self.makeLocationMapCell(property: property.name, location: location, from: table)
          }
        case "connectionType":
          value = (property.value as? ConnectionType)?.rawValue
        case "lastTimeWithActivity":
          value = (property.value as? Date)?.description(with: Locale.current)
          
        // Device properties
        case "loadType":
          value = (property.value as? LoadType)?.rawValue
        case "energyEfficiency":
          value = (property.value as? EnergyEfficiencyClass)?.rawValue
        case "identificationMethod":
          value = (property.value as? IdentificationMethod)?.rawValue
          
        // Box properties
        case "facilityAddress":
          let address = property.value as? Address
          value = address?.description
        case "usageProfile":
          value = (property.value as? UsageProfile)?.rawValue
        case "associatedTariffId":
          name = "associatedTariff"
          let tariffId = property.value as? String
          value = viewModel.activeSession.user.tariffs[tariffId!]?.description
        case "devices":
          if let devices = property.value as? [(String, String?)] {
            return self.makeMultiPropertyCell(property: property.name, subproperties: devices, from: table)
          }
        default:
          // Used in cases of id, label, firmwareVersion(Box), locationDescription(Device), manufacturer(Device), model(Device), serialNumber(Device
          value = property.value as? String
        }
        return self.makeSimplePropertyCell(property: name ?? property.name, value: value ?? (property.value as? String), from: table)
      }
      .disposed(by: self.disposeBag)
    
    cancelButton.rx.tap
         .bind {
           print("Dismissing Tariff manager view")
           self.dismiss(animated: true, completion: {
             viewModel.didDismiss.onNext(true)
           })
         }
         .disposed(by: self.disposeBag)
    
    doneButton.rx.tap
      .bind {
        print("Dismissing Details view")
        self.dismiss(animated: true, completion: {
          viewModel.didDismiss.onNext(true)
        })
      }
      .disposed(by: self.disposeBag)
  }
  
  private func makeSimplePropertyCell(property: String, value: String?, from table: UITableView) -> SimplePropertyCell {
    let cell = table.dequeueReusableCell(withIdentifier: "SimplePropertyCell")! as! SimplePropertyCell
    cell.propertyLabel.text = property.camelCaseToWords()
    if let stringValue = value {
      cell.valueLabel.text = stringValue
    } else {
      cell.valueLabel.text = "information not available"
      cell.valueLabel.font = cell.valueLabel.font.withSize(12)
      cell.propertyLabel.isEnabled = false
      cell.valueLabel.isEnabled = false
    }
    return cell
  }
  
  private func makeMultiPropertyCell(property: String, subproperties: [(id: String, label: String?)], from table: UITableView) -> MultiplePropertyCell {
    let cell = table.dequeueReusableCell(withIdentifier: "MultiplePropertyCell")! as! MultiplePropertyCell
    cell.propertyLabel.text = property.camelCaseToWords()
    
    let subpropertiesObservable = Observable.just(
      subproperties
    )
    
    subpropertiesObservable
      .bind(to: cell.subPropertiesTableView.rx.items(cellIdentifier: "SubpropertyCell")) { row, element, cell in
        if let deviceIdLabel = cell.viewWithTag(1) as? UILabel {
          deviceIdLabel.text = element.id
        }
        if let deviceLabelLabel = cell.viewWithTag(2) as? UILabel {
          deviceLabelLabel.text = element.label
        }
      }
      .disposed(by: disposeBag)
    
    return cell
  }
  
  private func makeLocationMapCell(property: String, location: Location, from table: UITableView) -> LocatablePropertyCell {
    let cell = table.dequeueReusableCell(withIdentifier: "LocatablePropertyCell")! as! LocatablePropertyCell
    
    cell.propertyLabel.text = property.camelCaseToWords()
    
    let location = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
    let region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.003, longitudeDelta: 0.003))
    cell.mapView.setRegion(region, animated: true)
    
    let annotation = MKPointAnnotation()
    annotation.coordinate = location
    annotation.title = "\(viewModel?.displayedVoltaElement.element.description ?? "")"
    cell.mapView.addAnnotation(annotation)
    cell.mapView.isZoomEnabled = true
    
    let scale = MKScaleView(mapView: cell.mapView)
    scale.scaleVisibility = .visible
    cell.mapView.addSubview(scale)
    
    let mapTapGesureRecognizer = UITapGestureRecognizer()
    cell.mapView.addGestureRecognizer(mapTapGesureRecognizer)
    mapTapGesureRecognizer.rx.event.bind(onNext: { recognizer in
      self.openInMaps(location: location)
    }).disposed(by: cell.disposeBag)
    
    return cell
  }
  
  private func openInMaps(location: CLLocationCoordinate2D) {
    print("Showing location in Maps App of:  \(viewModel?.displayedVoltaElement.element.description ?? "")")
    // TODO: Show label in Maps App
    let url  = NSURL(string: "http://maps.apple.com/?q=\(location.latitude),\(location.longitude)")
    if UIApplication.shared.canOpenURL(url! as URL) == true {
      UIApplication.shared.open(url! as URL)
    }
  }
}
