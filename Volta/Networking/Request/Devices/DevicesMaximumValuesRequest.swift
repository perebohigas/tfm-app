struct DevicesMaximumValuesRequest: ApiRequest {
  let method = RequestType.GET
  var pathVariable = "/maximum_values/boxes/"
  var queryParameters: [String : String]? = [String : String]()
  var bodyParameters: [String : String]? = nil
  
  init(boxId: String, from fromTimestamp: String, to toTimestamp: String) {
    pathVariable.append(boxId + "/devices")
    queryParameters?["from"] = fromTimestamp
    queryParameters?["to"] = toTimestamp
  }
  
  init(boxId: String, deviceId: String, from fromTimestamp: String, to toTimestamp: String) {
    pathVariable.append(boxId + "/devices/" + deviceId)
    queryParameters?["from"] = fromTimestamp
    queryParameters?["to"] = toTimestamp
  }
}
