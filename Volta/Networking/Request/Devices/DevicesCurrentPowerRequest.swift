struct DevicesCurrentPowerRequest: ApiRequest {
  let method = RequestType.GET
  var pathVariable = "/current_power/boxes/"
  var queryParameters: [String : String]? = nil
  var bodyParameters: [String : String]? = nil
  
  init(boxId: String) {
    pathVariable.append(boxId + "/devices")
  }
  
  init(boxId: String, deviceId: String) {
    pathVariable.append(boxId + "/devices/" + deviceId)
  }
}
