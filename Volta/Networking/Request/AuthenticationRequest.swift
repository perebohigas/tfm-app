struct AuthenticationRequest: ApiRequest {
  var method = RequestType.POST
  var pathVariable = "/sessions"
  var queryParameters: [String : String]? = nil
  var bodyParameters: [String : String]? = [String : String]()
  
  init(userName: String, password: String) {
    bodyParameters?["user_name"] = userName
    bodyParameters?["password"] = password
  }
}
