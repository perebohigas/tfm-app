import Foundation
import RxSwift

enum VoltaElementBehaviour {
  case inactiveSeconds
  case inactiveMinutes
  case inactiveHours
  case inactiveDays
  case inactiveWithoutData
  case activeConsuming
  case activeProducing
  case switchingConsumingOrInactiveSeconds
  case switchingProducingOrInactiveSeconds
}

struct MockServerApiClient: ApiClient {
  internal let baseURL: URL = URL(string: "http://mock_server.org")!
  internal let mockSystem: [String : [(id: String, behaviour: VoltaElementBehaviour)]] = [
    "1": [
      ("1", .activeConsuming),                        ("2", .activeConsuming),
      ("3", .inactiveHours),                          ("4", .inactiveDays),
      ("5", .inactiveWithoutData),                    ("6", .inactiveMinutes),
      ("7", .inactiveDays),                           ("8", .activeConsuming),
      ("9", .activeConsuming),                        ("10", .inactiveHours),
      ("11", .activeConsuming),                       ("12", .switchingConsumingOrInactiveSeconds),
      ("13", .switchingConsumingOrInactiveSeconds),   ("14", .inactiveHours),
      ("15", .switchingConsumingOrInactiveSeconds),   ("16", .activeConsuming),
      ("17", .inactiveDays),                          ("18", .inactiveMinutes),
      ("19", .inactiveHours),                         ("20", .activeConsuming),
      ("21", .activeProducing)
    ],
    "2": [
      ("A", .activeProducing),                        ("B", .switchingProducingOrInactiveSeconds),
      ("C", .inactiveDays),                           ("D", .switchingConsumingOrInactiveSeconds),
      ("E", .activeConsuming)
    ],
    "3": [
      ("ca-c", .activeConsuming),                     ("ca-d", .activeConsuming),
      ("sa-c", .activeConsuming),                     ("sa-d", .switchingConsumingOrInactiveSeconds),
      ("pm-c", .inactiveHours),                       ("pm-d", .activeConsuming),
      ("tl-c", .activeConsuming),                     ("tl-d", .activeConsuming),
      ("sd1-c", .switchingConsumingOrInactiveSeconds),("sd1-d", .activeConsuming),
      ("sd2-c", .activeConsuming),                    ("sd2-d", .switchingConsumingOrInactiveSeconds),
      ("sd3-c", .inactiveHours),                      ("sd3-d", .activeConsuming),
      ("ceo-c", .activeConsuming),                    ("ceo-d", .activeConsuming),
      ("do-c", .activeConsuming),                     ("do-d", .activeConsuming),
      ("pr-1", .activeConsuming),                     ("rt-1", .switchingConsumingOrInactiveSeconds),
      ("sw-1", .activeConsuming),                     ("svr-1", .activeConsuming),
      ("cm-1", .switchingConsumingOrInactiveSeconds), ("fd-1", .activeConsuming),
      ("mw-1", .activeConsuming),                     ("wh-1", .switchingConsumingOrInactiveSeconds),
      ("tv-1", .inactiveHours),                       ("tv-2", .activeConsuming),
      ("lg-1", .activeConsuming),                     ("lg-2", .activeConsuming),
      ("lg-3", .inactiveHours),                       ("lg-4", .activeConsuming)
    ],
    "4": [
      ("I", .inactiveMinutes),                        ("II", .inactiveHours),
      ("III", .inactiveDays),                         ("IV", .inactiveMinutes),
      ("V", .inactiveHours),                          ("VI", .inactiveDays),
      ("VII", .inactiveMinutes),                      ("VIII", .inactiveHours),
      ("IX", .inactiveDays),                          ("X", .inactiveMinutes),
      ("XI", .inactiveHours),                         ("XII", .inactiveDays),
      ("XIII", .inactiveHours),                       ("XIV", .inactiveDays),
      ("XV", .inactiveMinutes),                       ("XVI", .inactiveHours),
      ("XVII", .inactiveDays),                        ("XVIII", .inactiveMinutes),
      ("XIX", .inactiveHours),                        ("XX", .inactiveDays),
      ("XXI", .inactiveHours)
    ]
  ]
  
  func request<T: ApiResponse>(with apiRequest: ApiRequest) -> Single<(Int, T)> {
    return Single<(Int, T)>.create { single in
      let request = apiRequest.request(with: self.baseURL)
      
      var consoleMessage = "\(Date()) -> Sended the \(request.httpMethod ?? "") request to mock server - \(request.description)"
      if let requestBody = request.httpBody {
        consoleMessage.append("\nWith the body: " + String(decoding: requestBody, as: UTF8.self))
      }
      print(consoleMessage)
      
      var response: T? = nil
      var jsonFile: String? = nil
      var statusCode: Int = 0
      
      switch apiRequest {
      case is AuthenticationRequest:
        (statusCode, jsonFile) = self.mockAuthentication(apiRequest as! AuthenticationRequest)
        
      // Information
      case is BoxesInformationRequest:
        (statusCode, jsonFile) = (200, "information_multiple_boxes")
      case is DevicesInformationRequest:
        (statusCode, jsonFile) = self.mockDevicesInformation(apiRequest as! DevicesInformationRequest)
        
      // Current Power
      case is BoxesCurrentPowerRequest:
        (statusCode, response) = (200, self.mockCurrentPowerResponseBoxes() as? T)
      case is DevicesCurrentPowerRequest:
        (statusCode, response) = (200, self.mockCurrentPowerResponseDevices(apiRequest as! DevicesCurrentPowerRequest) as? T)
        
        
        // TODO: implement Accumulated Power mocked responses
      // TODO: implement Maximum Values mocked responses
      default:
        jsonFile = String()
      }
      
      if let responseObject = response {
        // Using a mocked response
        print("\(Date()) <- Mocked a \(statusCode) response of type \(type(of: responseObject))")
        
        single(.success((200,responseObject)))
      } else if let path = Bundle.main.path(forResource: jsonFile, ofType: "json") {
        // Using a JSON file
        do {
          let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
          
          let decoder = JSONDecoder()
          decoder.dateDecodingStrategy = .iso8601
          decoder.keyDecodingStrategy = .convertFromSnakeCase
          
          let responseBody: T = try decoder.decode(T.self, from: data )
          
          print("\(Date()) <- Mocked a \(statusCode) response from the file: \(jsonFile ?? "")")
          
          single(.success((statusCode,responseBody)))
        } catch {
          single(.error(error))
        }
      } else {
        print("Response could not be mocked")
      }
      
      return Disposables.create {}
    }
  }
  
  // AUTHENTICATION
  func mockAuthentication(_ request: AuthenticationRequest) -> (status: Int, jsonFile: String) {
    if let password = request.bodyParameters?["password"] {
      if password != "wrong" {
        return (200, "session_start_success")
      }
    }
    return (401, "session_start_failure")
  }
  
  // INFORMATION
  // Boxes
  func mockDevicesInformation(_ request: DevicesInformationRequest) -> (status: Int, jsonFile: String) {
    let requestComponents = request.pathVariable.components(separatedBy: "/")
    let boxId: String = requestComponents[3]
    var jsonFile: String = "information_multiple_devices_box_"
    jsonFile.append(boxId)
    return (200, jsonFile)
  }
  
  // CURRENT POWER
  // Boxes
  func mockCurrentPowerResponseBoxes() -> BoxesCurrentPowerResponse {
    var mockBoxesCurrentPowerResponse = [SingleBoxCurrentPowerResponse]()
    for boxId in mockSystem.keys {
      mockBoxesCurrentPowerResponse.append(mockCurrentPowerResponseSingleBox(boxId))
    }
    return BoxesCurrentPowerResponse(responseTimestamp: Date(), data: mockBoxesCurrentPowerResponse)
  }
  
  func mockCurrentPowerResponseSingleBox(_ boxId: String) -> SingleBoxCurrentPowerResponse {
    // Office case
    let activeOrInactive: Bool = Bool.random()
    // Workshop case (maybe Office)
    var mockActivePower: Double = 0
    var mockApparentPower: Double = 0
    var mockActiveDevices: Int = 0
    if boxId == "1" || boxId == "3" && activeOrInactive{
      // House case (maybe Office)
      mockActivePower = Double.random(in: -3.450 ... 0)
      mockApparentPower = Double.random(in: -3.749 ... mockActivePower)
      mockActiveDevices = Int.random(in: 0 ... 18)
    } else if boxId == "2" {
      // Wind farm case
      mockActivePower = Double.random(in: 0 ... 1.800)
      mockApparentPower = Double.random(in: mockActivePower ... 1.928)
      mockActiveDevices = Int.random(in: 0 ... 4)
    }
    let mockCurrentPower = CurrentPowerBox(timestamp: Date(), activeValue: mockActivePower, apparentValue: mockApparentPower, activeDevices: mockActiveDevices)
    var mockLastTimeWithActivity: Date? = nil
    if mockActivePower == 0 && mockApparentPower == 0 {
      mockLastTimeWithActivity = Date(timeIntervalSinceNow: Double.random(in: -1209600 ... 0))
    }
    return SingleBoxCurrentPowerResponse(boxId: boxId, currentPower: mockCurrentPower, lastTimeActive: mockLastTimeWithActivity )
  }
  
  // Devices
  func mockCurrentPowerResponseDevices(_ request: DevicesCurrentPowerRequest) -> DevicesCurrentPowerResponse {
    let requestComponents = request.pathVariable.components(separatedBy: "/")
    let boxId: String = requestComponents[3]
    var mockDevicesCurrentPowerResponse = [SingleDeviceCurrentPowerResponse]()
    for device in mockSystem[boxId]! {
      mockDevicesCurrentPowerResponse.append(mockCurrentPowerResponseSingleDevice(device))
    }
    return DevicesCurrentPowerResponse(responseTimestamp: Date(), data: mockDevicesCurrentPowerResponse)
  }
  
  func mockCurrentPowerResponseSingleDevice(_ device: (id: String, behaviour: VoltaElementBehaviour)) -> SingleDeviceCurrentPowerResponse {
    var newBehaviour: VoltaElementBehaviour? = nil
    let activeOrInactive: Bool = Bool.random()
    // Proceed for switching cases
    switch device.behaviour {
    case .switchingConsumingOrInactiveSeconds:
      if activeOrInactive {
        newBehaviour = .activeConsuming
      } else {
        newBehaviour = .inactiveSeconds
      }
    case .switchingProducingOrInactiveSeconds:
      if activeOrInactive {
        newBehaviour = .activeProducing
      } else {
        newBehaviour = .inactiveSeconds
      }
    default:
      break
    }
    let (mockCurrentPower, mockLastTimeWithActivity) = mockCurrentPowerDevice(newBehaviour ?? device.behaviour)
    return SingleDeviceCurrentPowerResponse(deviceId: device.id, currentPower: mockCurrentPower, lastTimeActive: mockLastTimeWithActivity )
  }
  func mockCurrentPowerDevice(_ behaviour: VoltaElementBehaviour) -> (CurrentPowerDevice, Date?) {
    // Proceed for inactive/active cases
    var mockActivePower: Double = 0
    var mockApparentPower: Double = 0
    var mockLastTimeWithActivity: Date? = nil
    switch behaviour {
    case .inactiveSeconds:
      // from 1 second to 59 seconds
      mockLastTimeWithActivity = Date(timeIntervalSinceNow: Double.random(in: -59 ... -1))
    case .inactiveMinutes:
      // from 1 minute to 59 minutes and 59 seconds
      mockLastTimeWithActivity = Date(timeIntervalSinceNow: Double.random(in: -3599 ... -60))
    case .inactiveHours:
      // from 1 hour to 23 hours and 59 minutes and 59 seconds
      mockLastTimeWithActivity = Date(timeIntervalSinceNow: Double.random(in: -86399 ... -3600))
    case .inactiveDays:
      // from 1 day to 90 days
      mockLastTimeWithActivity = Date(timeIntervalSinceNow: Double.random(in: -7776000 ... -86400))
    case .activeConsuming:
      mockActivePower = Double.random(in: -1.350 ... 0)
      mockApparentPower = Double.random(in: -1.549 ... mockActivePower)
    case .activeProducing:
      mockActivePower = Double.random(in: 0 ... 1.350)
      mockApparentPower = Double.random(in: mockActivePower ... 1.549)
    default:
      break
    }
    return (CurrentPowerDevice(timestamp: Date(), activeValue: mockActivePower, apparentValue: mockApparentPower), mockLastTimeWithActivity)
  }
  
}
