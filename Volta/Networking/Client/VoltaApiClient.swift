import Foundation
import RxSwift

struct VoltaApiClient: ApiClient {
  internal let baseURL = URL(string: "https://b5b75213-ef81-4dac-8412-558f4d1ac0d7.mock.pstmn.io")!
  
  func request<T: ApiResponse>(with apiRequest: ApiRequest) -> Single<(Int, T)> {
    return Single<(Int, T)>.create { single in
      let request = apiRequest.request(with: self.baseURL)
      
      var consoleMessage = "\(Date()) -> Sended a \(request.httpMethod ?? "") request to mock server - \(request.description)"
      if let requestBody = request.httpBody {
        consoleMessage.append("\nWith the body: " + String(decoding: requestBody, as: UTF8.self))
      }
      print(consoleMessage)
      
      let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        do {
          let decoder = JSONDecoder()
          decoder.dateDecodingStrategy = .iso8601
          decoder.keyDecodingStrategy = .convertFromSnakeCase
          
          let statusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
          
          guard let responseData = data else { return }
          
          let responseBody: T = try decoder.decode(T.self, from: responseData)
          
          print("\(Date()) <- Received a \(statusCode) response, from which objects of type \(type(of: responseBody.data)) have been decoded, containing the following body:\n" + String(decoding: responseData, as: UTF8.self))
          
          single(.success((statusCode, responseBody)))
        } catch let error {
          single(.error(error))
        }
      }
      task.resume()
      
      return Disposables.create {
        task.cancel()
      }
    }
  }
}
