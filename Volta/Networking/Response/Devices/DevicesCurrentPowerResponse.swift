import Foundation

struct DevicesCurrentPowerResponse: ApiResponse {
  let responseTimestamp: Date
  let data: [SingleDeviceCurrentPowerResponse]
}

struct SingleDeviceCurrentPowerResponse: Codable {
  let deviceId: String
  let currentPower: CurrentPowerDevice
  let lastTimeActive: Date?
}
