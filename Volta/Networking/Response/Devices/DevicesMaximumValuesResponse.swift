import Foundation

struct DevicesMaximumValuesResponse: ApiResponse {
  let responseTimestamp: Date
  let data: [SingleDeviceMaximumValuesResponse]
}

struct SingleDeviceMaximumValuesResponse: Codable {
  let deviceId: String
  let maximumValues: MaximumValues
}
