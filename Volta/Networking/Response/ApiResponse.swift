import Foundation

protocol ApiResponse: Codable {
  associatedtype ResponseData
  
  var responseTimestamp: Date { get }
  var data: ResponseData { get }
}
