import UIKit

// TODO: Add colors to Assets.xcassets providing dark and white alternatives
enum AppColors {
  static let mainYellow = UIColor(red: 0xBC / 0xFF, green: 0xBD / 0xFF, blue: 0x77 / 0xFF, alpha: 1)
  static let mainLightGreen = UIColor(red: 0x5A / 0xFF, green: 0x9B / 0xFF, blue: 0x83 / 0xFF, alpha: 1)
  static let mainDarkGreen = UIColor(red: 0x16 / 0xFF, green: 0x5C / 0xFF, blue: 0x5F / 0xFF, alpha: 1)
  static let mainLightBlack = UIColor(red: 0x1A / 0xFF, green: 0x1D / 0xFF, blue: 0x22 / 0xFF, alpha: 1)
  static let mainDarkBlack = UIColor(red: 0x0E / 0xFF, green: 0x0E / 0xFF, blue: 0x0E / 0xFF, alpha: 1)
  static let consumingBlue = UIColor(red: 0x8d / 0xFF, green: 0xb0 / 0xFF, blue: 0xcc / 0xFF, alpha: 1)
}
