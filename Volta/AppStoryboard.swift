import Foundation

enum AppStoryboard: String {
  case signIn = "SignIn"
  case boxes = "Boxes"
  case tariffs = "Tariffs"
  case devices = "Devices"
  case details = "Details"
  case tariffManager = "TariffManager"
}
