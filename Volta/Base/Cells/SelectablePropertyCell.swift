import RxSwift
import UIKit

class SelectablePropertyCell: UITableViewCell {
  var disposeBag = DisposeBag()
  
  @IBOutlet weak var propertyLabel: UILabel!
  @IBOutlet weak var propertySelector: UISegmentedControl!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.disposeBag = DisposeBag()
  }
}
