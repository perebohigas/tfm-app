import CoreLocation

struct Box: VoltaElement, Codable {
  let id: String
  var label: String?
  
  var firmwareVersion: String?
  var connectionType: ConnectionType?
  
  var location: Location?
  var facilityAddress: Address?
  
  var associatedTariffId: String?
  var lastTimeActive: Date?
  
  var devices = [String: Device]()
  var currentPower = [CurrentPowerBox]()
  var accumulatedPower = [AccumulatedEnergy]()
  var maximumValues = [MaximumValues]()
  
  var description: String {
    return ("Box - " + (self.label ?? self.id))
  }
  
  func getLastCurrentPowerValue(usage: UsageProfile) -> (power: PowerValue, timestamp: Date)? {
    let power: PowerValue
    let timestamp: Date
    guard let currentPower = self.currentPower.last else { return nil }
    switch usage {
    case .household:
      power = currentPower.activePower
      timestamp = currentPower.measurementTimestamp
    case .industrial:
      power = currentPower.apparentPower
      timestamp = currentPower.measurementTimestamp
    }
    return (power, timestamp)
  }
  
  func getLastCurrentPowerValue(usage: UsageProfile, deviceId: String) -> (power: PowerValue, timestamp: Date)? {
    let power: PowerValue
    let timestamp: Date
    guard let device = self.devices[deviceId] else { return nil }
    guard let currentPower = device.currentPower.last else { return nil }
    switch usage {
    case .household:
      power = currentPower.activePower
      timestamp = currentPower.measurementTimestamp
    case .industrial:
      power = currentPower.apparentPower
      timestamp = currentPower.measurementTimestamp
    }
    return (power, timestamp)
  }
}

// Declaring CodingKeys enum to conform Codable
extension Box {
  private enum CodingKeys: String, CodingKey {
    case id = "boxId"
    case label
    case location
    case connectionType
    case facilityAddress
    case firmwareVersion
  }
}

struct CurrentPowerBox: CurrentData, PowerData, Codable {
  // Protocol attributtes
  let measurementTimestamp: Date
  let activePower: PowerValue
  // reactivePower value computed on init
  let reactivePower: PowerValue
  let apparentPower: PowerValue
  
  // Additional attributes
  let activeDevices: Int
  let powerFactor: Double
  
  init(timestamp: Date, activeValue: Double, apparentValue: Double, activeDevices: Int) {
    self.measurementTimestamp = timestamp
    self.activePower = PowerValue(value: activeValue, type: .active)
    self.apparentPower = PowerValue(value: apparentValue, type: .apparent)
    self.activeDevices = activeDevices
    
    // Computed properties
    let reactivePowerValue = sqrt(pow(apparentValue, 2) - pow(activeValue, 2))
    self.reactivePower = PowerValue(value: reactivePowerValue,type: .reactive)
    self.powerFactor = (activeValue/apparentValue)
  }
}

extension CurrentPowerBox {
  enum CodingKeys: String, CodingKey {
    case measurementTimestamp
    case activePower
    case apparentPower
    case activeDevices
  }
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    let measurementTimestamp = try values.decode(Date.self, forKey: .measurementTimestamp)
    let activePowerValue = try values.decode(Double.self, forKey: .activePower)
    let apparentPowerValue = try values.decode(Double.self, forKey: .apparentPower)
    let activeDevices = try values.decode(Int.self, forKey: .activeDevices)
    
    self.init(timestamp: measurementTimestamp, activeValue: activePowerValue, apparentValue: apparentPowerValue, activeDevices: activeDevices)
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    
    try container.encode(measurementTimestamp, forKey: .measurementTimestamp)
    try container.encode(activePower.value, forKey: .activePower)
    try container.encode(apparentPower.value, forKey: .apparentPower)
    try container.encode(activeDevices, forKey: .activeDevices)
  }
}

struct Address: Codable, CustomStringConvertible {
  var subLocality: String?
  var street: String
  var houseNumber: String?
  var subAdministrativeArea: String?
  var city: String
  var state: String?
  var postalCode: String
  var country: String
  // isoCountryCode value using the ISO 3166-1 alpha-2 standard
  var isoCountryCode: String
  
  var description: String {
    let addressString: String =
      ((self.subLocality ?? "").isEmpty ? "" : (self.subLocality! + "\n")) +
        self.street + ((self.houseNumber ?? "").isEmpty ? "" : (", " + self.houseNumber!)) + "\n" +
        self.postalCode + " " + self.city + ((self.subAdministrativeArea ?? "").isEmpty ? "" : (" (" + self.subAdministrativeArea! + ")")) + "\n" +
        ((self.state ?? "").isEmpty ? "" : (self.state! + ", ")) + self.country + " (" + self.isoCountryCode + ")"
    
    return addressString
  }
}
