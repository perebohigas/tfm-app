import Foundation

struct PowerValue {
  var value: Double
  var unit: PowerType
  
  init(value: Double, type: PowerType) {
    self.value = value
    self.unit = type
  }
}

struct EnergyValue {
  var value: Double
  var unit: EnergyType
  
  init(value: Double, type: EnergyType) {
    self.value = value
    self.unit = type
  }
}

protocol PowerData {
  // Active power measured in kW
  var activePower: PowerValue { get }
  // Reactive power measured in VAr
  var reactivePower: PowerValue { get }
  // Apparent power measured in VA
  var apparentPower: PowerValue { get }
}

protocol EnergyData {
  // Active energy measured in kWh
  var activeEnergy: EnergyValue { get }
  // Reactive energy measured in VArh
  var reactiveEnergy: EnergyValue { get }
  // Apparent energy measured in VAh
  var apparentEnergy: EnergyValue { get }
}

protocol CurrentData: Comparable {
  // measurementTimestamp value using the ISO 8601 standard
  var measurementTimestamp: Date { get }
}

// Implementing the == and the < operators to conform Comparable
extension CurrentData {
  static func ==(lhs: Self, rhs: Self) -> Bool {
    return lhs.measurementTimestamp == rhs.measurementTimestamp
  }
  
  static func <(lhs: Self, rhs: Self) -> Bool {
    return lhs.measurementTimestamp < rhs.measurementTimestamp
  }
}

protocol IntervalData: Comparable {
  // fromTimestamp value using the ISO 8601 standard
  var fromTimestamp: Date { get }
  // toTimestamp value using the ISO 8601 standard
  var toTimestamp: Date { get }
  var timeInterval: DateInterval { get }
}

// Implementing the == and the < operators to conform Comparable
extension IntervalData {
  static func ==(lhs: Self, rhs: Self) -> Bool {
    return lhs.toTimestamp == rhs.toTimestamp && lhs.fromTimestamp == rhs.fromTimestamp
  }
  
  static func <(lhs: Self, rhs: Self) -> Bool {
    if lhs.toTimestamp != rhs.toTimestamp {
      // compare their end times
      return lhs.toTimestamp < rhs.toTimestamp
    } else {
      // both have the same end time, compare their start time
      return lhs.fromTimestamp < rhs.fromTimestamp
    }
  }
}

struct AccumulatedEnergy: IntervalData, EnergyData, Codable {
  // Protocol attributtes
  let fromTimestamp: Date
  let toTimestamp: Date
  // timeInterval value computed on init
  let timeInterval: DateInterval
  let activeEnergy: EnergyValue
  let reactiveEnergy: EnergyValue
  let apparentEnergy: EnergyValue
  
  // Additional attributes
  let timeActive: TimeInterval
}

extension AccumulatedEnergy {
  enum CodingKeys: String, CodingKey {
    case fromTimestamp
    case toTimestamp
    case activeEnergy
    case reactiveEnergy
    case apparentEnergy
    case timeActive
  }
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    fromTimestamp = try values.decode(Date.self, forKey: .fromTimestamp)
    toTimestamp = try values.decode(Date.self, forKey: .toTimestamp)
    let activePowerValue = try values.decode(Double.self, forKey: .activeEnergy)
    activeEnergy = EnergyValue(value: activePowerValue, type: .active)
    let reactivePowerValue = try values.decode(Double.self, forKey: .reactiveEnergy)
    reactiveEnergy = EnergyValue(value: reactivePowerValue, type: .reactive)
    let apparentPowerValue = try values.decode(Double.self, forKey: .apparentEnergy)
    apparentEnergy = EnergyValue(value: apparentPowerValue, type: .apparent)
    timeActive = try values.decode(TimeInterval.self, forKey: .timeActive)
    
    // Computed properties
    timeInterval = DateInterval(start:fromTimestamp, end: toTimestamp)
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    
    try container.encode(fromTimestamp, forKey: .fromTimestamp)
    try container.encode(toTimestamp, forKey: .toTimestamp)
    try container.encode(activeEnergy.value, forKey: .activeEnergy)
    try container.encode(reactiveEnergy.value, forKey: .reactiveEnergy)
    try container.encode(apparentEnergy.value, forKey: .apparentEnergy)
    try container.encode(timeActive, forKey: .timeActive)
  }
}

struct MaximumValues: IntervalData, PowerData, Codable {
  // Protocol attributtes
  let fromTimestamp: Date
  let toTimestamp: Date
  // timeInterval value computed on init
  let timeInterval: DateInterval
  let activePower: PowerValue
  let reactivePower: PowerValue
  let apparentPower: PowerValue
  
  // Additional attributes
  let maxPeakCurrent: Double
  let minPowerFactor: Double
}

extension MaximumValues {
  enum CodingKeys: String, CodingKey {
    case fromTimestamp
    case toTimestamp
    case activePower
    case reactivePower
    case apparentPower
    case maxPeakCurrent
    case minPowerFactor
  }
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    fromTimestamp = try values.decode(Date.self, forKey: .fromTimestamp)
    toTimestamp = try values.decode(Date.self, forKey: .toTimestamp)
    let activePowerValue = try values.decode(Double.self, forKey: .activePower)
    activePower = PowerValue(value: activePowerValue, type: .active)
    let reactivePowerValue = try values.decode(Double.self, forKey: .reactivePower)
    reactivePower = PowerValue(value: reactivePowerValue, type: .reactive)
    let apparentPowerValue = try values.decode(Double.self, forKey: .apparentPower)
    apparentPower = PowerValue(value: apparentPowerValue, type: .apparent)
    maxPeakCurrent = try values.decode(TimeInterval.self, forKey: .maxPeakCurrent)
    minPowerFactor = try values.decode(TimeInterval.self, forKey: .minPowerFactor)
    
    // Computed properties
    timeInterval = DateInterval(start:fromTimestamp, end: toTimestamp)
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    
    try container.encode(fromTimestamp, forKey: .fromTimestamp)
    try container.encode(toTimestamp, forKey: .toTimestamp)
    try container.encode(activePower.value, forKey: .activePower)
    try container.encode(reactivePower.value, forKey: .reactivePower)
    try container.encode(apparentPower.value, forKey: .apparentPower)
    try container.encode(maxPeakCurrent, forKey: .maxPeakCurrent)
    try container.encode(minPowerFactor, forKey: .minPowerFactor)
  }
}

enum PowerType: String {
  case active = "kW"
  case reactive = "kVAr"
  case apparent = "kVA"
}

enum EnergyType: String {
  case active = "kWh"
  case reactive = "kVArh"
  case apparent = "kVAh"
}
