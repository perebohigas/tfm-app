import CoreLocation

protocol VoltaElement: Comparable, CustomStringConvertible {
  associatedtype CurrentPower
  
  var id: String { get }
  var label: String? { get set }
  
  var location: Location? { get set }
  
  var connectionType: ConnectionType? { get set }
  var lastTimeActive: Date? { get set }
  
  var currentPower: [CurrentPower] { get set }
  var accumulatedPower: [AccumulatedEnergy] { get set }
  var maximumValues: [MaximumValues] { get set }
  
  // Declaring description property to conform CustomStringConvertible
  var description: String { get }
  
  // TODO: Add a function to get currentPower in a specific time by interpolating
}

// Implementing the == and the < operators to conform Comparable
extension VoltaElement {
  static func ==(lhs: Self, rhs: Self) -> Bool {
    return lhs.id == rhs.id
  }
  
  static func <(lhs: Self, rhs: Self) -> Bool {
    if lhs.label != nil && rhs.label != nil {
      // both have a label, compare their labels
      return lhs.label! < rhs.label!
    } else if lhs.label != nil {
      // first has a label
      return true
    } else if rhs.label != nil {
      // second has a label
      return false
    } else {
      // none of them has a label, compare their ids
      return lhs.id < rhs.id
    }
  }
}

extension VoltaElement {
  func getInactiveTime() -> TimeInterval? {
    if let lastActive = self.lastTimeActive {
      return Date().timeIntervalSince(lastActive)
    } else {
      return nil
    }
  }
}

struct Location: Codable {
  var latitude: CLLocationDegrees
  var longitude: CLLocationDegrees
}

enum ConnectionType: String, Codable {
  case singlePhase = "single-phase"
  case threePhase = "three-phase"
}
